import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.sql.*;
import java.util.*;

public class Addbook extends JFrame implements ActionListener
{
	JLabel userLabel, passLabel, eNameLabel, phoneLabel, roleLabel, salaryLabel,imgLabel;
	JTextField userTF, passTF, phoneTF1, phoneTF2, eNameTF, salaryTF,roleTF;
	JComboBox roleCombo;
	JButton autoPassBtn, addBtn, backBtn, logoutBtn,updateBtn;
	ImageIcon img;
	Font f1;
	JPanel panel;
	
	String userId;
	
	public Addbook(String userId)
	{
		super("Library Management System - Add New Book");
		
		this.setSize(800, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.userId = userId;
		
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		int jFrameWidth = this.getSize().width;
		int jFrameHeight = this.getSize().height;
		
		int locationX = (d.width-jFrameWidth)/2;
		int locationY = (d.height-jFrameHeight)/2;
		this.setLocation(locationX,locationY);
		
		panel = new JPanel();
		panel.setLayout(null);
		
		f1 = new Font("Calibri",Font.BOLD,20);
		
		logoutBtn = new JButton("Logout");
		logoutBtn.setBounds(600, 50, 100, 30);
		logoutBtn.addActionListener(this);
		panel.add(logoutBtn);
		
		/*refreshBtn = new JButton("Refresh");
		refreshBtn.setBounds(250, 100, 275, 30);
		refreshBtn.addActionListener(this);
		panel.add(refreshBtn);*/
		
		userLabel = new JLabel("BOOK ID : ");
		userLabel.setBounds(230, 100, 200, 30);
		userLabel.setFont(f1);
		panel.add(userLabel);
		
		Random r = new Random();
		
		userTF = new JTextField("b"+r.nextInt(999));
		userTF.setBounds(400, 100, 120, 30);
		userTF.setEnabled(false);
		panel.add(userTF);
		
		/*loadBtn = new JButton("Load");
		loadBtn.setBounds(550, 150, 150, 30);
		loadBtn.addActionListener(this);
		panel.add(loadBtn);*/
		
		eNameLabel = new JLabel("BOOK Name : ");
		eNameLabel.setBounds(230, 150, 200, 30);
		eNameLabel.setFont(f1);
		panel.add(eNameLabel);
		
		eNameTF = new JTextField();
		eNameTF.setBounds(400, 150, 120, 30);
		panel.add(eNameTF);
		
		phoneLabel = new JLabel("Author Name : ");
		phoneLabel.setBounds(230, 200, 200, 30);
		phoneLabel.setFont(f1);
		panel.add(phoneLabel);
		
		phoneTF2 = new JTextField();
		phoneTF2.setBounds(400, 200, 120, 30);
		panel.add(phoneTF2);
		
		roleLabel = new JLabel("Publication Year ");
		roleLabel.setBounds(230, 250, 200, 30);
		roleLabel.setFont(f1);
		panel.add(roleLabel);
		
		roleTF = new JTextField();
		roleTF.setBounds(400, 250, 120, 30);
		panel.add(roleTF);
		
		salaryLabel = new JLabel("Available Quantity: ");
		salaryLabel.setBounds(230, 300, 200, 30);
		salaryLabel.setFont(f1);
		panel.add(salaryLabel);
		
		salaryTF = new JTextField();
		salaryTF.setBounds(400, 300, 120, 30);
		panel.add(salaryTF);
		
		updateBtn = new JButton("Add");
		updateBtn.setBounds(200, 350, 120, 30);
		//updateBtn.setEnabled(false);
		updateBtn.addActionListener(this);
		panel.add(updateBtn);
		
		/*delBtn = new JButton("Delete");
		delBtn.setBounds(350, 400, 120, 30);
		delBtn.setEnabled(false);
		delBtn.addActionListener(this);
		panel.add(delBtn);
		*/
		backBtn = new JButton("Back");
		backBtn.setBounds(500, 350, 120, 30);
		backBtn.addActionListener(this);
		panel.add(backBtn);
		
		img =  new ImageIcon("bg op2.jpg");
		imgLabel = new JLabel(img);
		imgLabel.setBounds(0,0,800,500);
		panel.add(imgLabel);
		
		this.add(panel);
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		String text = ae.getActionCommand();
		
		if(text.equals(backBtn.getText()))
		{
			EmployeeHome me = new EmployeeHome(userId);
			me.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(logoutBtn.getText()))
		{
			Login lg = new Login();
			lg.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(updateBtn.getText()))
		{
			insertIntoDB();
		}
		else{}
	}
	public void insertIntoDB()
	{
		String newId = userTF.getText();
		//String newPass = ename.getText();
		String eName = eNameTF.getText();
		String author = phoneTF2.getText();
		String role = roleTF.getText();
		int availableQ = Integer.parseInt(salaryTF.getText());
		String phnNo=null;
		
		
		String query1 = "INSERT INTO book VALUES ('"+newId+"','"+eName+"','"+author+"','"+role+"',"+availableQ+");";
		//String query2 = "INSERT INTO login VALUES ('"+newId+"','"+newPass+"',"+status+");";
		System.out.println(query1);
		//System.out.println(query2);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("ok");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22", "root", "");
			System.out.println("ok");
			Statement stm = con.createStatement();
			System.out.println("ok");
			stm.execute(query1);
			System.out.println("ok");
			//stm.execute(query2);
			System.out.println("ok");
			stm.close();
			con.close();
			JOptionPane.showMessageDialog(this, "Success !!!");
		}
        catch(Exception ex)
		{
			JOptionPane.showMessageDialog(this, "Oops !!!..invalid id");
        }
    }	
}