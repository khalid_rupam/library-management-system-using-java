import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;
import java.awt.*;

public class ChangePasswordCustomer extends JFrame implements ActionListener
{
	JLabel oldPassLabel, newPassLabel,im;
	JTextField oldPassTF, newPassTF;
	JButton changeBtn, backBtn, logoutBtn;
	JPanel panel;
	private ImageIcon i;
	String userId;
	private Font       f1,f2;
	
	public ChangePasswordCustomer(String userId)
	{
		super("                                      Change Password Window                      ");
		
		this.userId = userId;
		this.setSize(800,450);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		int jFrameWidth = this.getSize().width;
		int jFrameHeight = this.getSize().height;
		
		int locationX = (d.width-jFrameWidth)/2;
		int locationY = (d.height-jFrameHeight)/2;
		this.setLocation(locationX,locationY);
		
		panel = new JPanel();
		panel.setLayout(null);
		
		

        f1=new Font("Calibri",Font.BOLD,20);
		f2=new Font("Calibri",Font.BOLD,20);
		
		
		logoutBtn = new JButton("Logout");
		logoutBtn.setBounds(600, 50, 100, 30);
		logoutBtn.addActionListener(this);
		panel.add(logoutBtn);
		
		oldPassLabel = new JLabel("Old Password: ");
		oldPassLabel.setBounds(280, 150, 160, 30);
		oldPassLabel.setFont(f1);
		panel.add(oldPassLabel);
		
		oldPassTF = new JTextField();
		oldPassTF.setBounds(420, 150, 110, 30);
		panel.add(oldPassTF);
		
		newPassLabel = new JLabel("New Password: ");
		newPassLabel.setBounds(280, 200, 170, 30);
		newPassLabel.setFont(f2);
		panel.add(newPassLabel);
		
		newPassTF = new JPasswordField();
		newPassTF.setBounds(420, 200, 110, 30);
		panel.add(newPassTF);
		
		
		changeBtn = new JButton("Change");
		changeBtn.setBounds(330, 280, 80, 30);
		changeBtn.addActionListener(this);
		panel.add(changeBtn);
		
		
		backBtn = new JButton("Back");
		backBtn.setBounds(430, 280, 80, 30);
		backBtn.addActionListener(this);
		panel.add(backBtn);
		
		i = new ImageIcon("WhatsApp Image.jpeg");
		im = new JLabel(i);
		im.setBounds(0,0,800,450);
		panel.add(im);
		
		this.add(panel);
	}
	public void actionPerformed(ActionEvent ae)
	{   String str = ae.getActionCommand();
		if(str.equals(changeBtn.getText()))
		{
			changepass();
		}
		else if(str.equals(backBtn.getText()))
		{
			CustomerHome ch = new CustomerHome(userId);
			ch.setVisible(true);
			this.setVisible(false);
		}
		else if(str.equals(logoutBtn.getText()))
		{
			Login l = new Login();
			l.setVisible(true);
			this.setVisible(false);
		}
		
	}
	public void changepass()
	{
		String loadId = userId;
		String query = "SELECT `userId`, `password`, `status` FROM `login` WHERE `userId`='"+loadId+"';";    
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			
			System.out.println("ok");
			boolean flag = false;
			System.out.println("ok");
						
			while(rs.next())
			{
				String password = rs.getString("password");
				String s = newPassTF.getText();
				if( password.equals(oldPassTF.getText()))
				{
					flag=true;
					String query1 = "UPDATE login SET password = '"+s+"' WHERE userId='"+userId+"'";	
					Connection con1=null;//for connection
					Statement st1 = null;//for query execution
					System.out.println(query1);
				try
				{
					Class.forName("com.mysql.jdbc.Driver");//load driver
					con1 = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22","root","");
					st1 = con.createStatement();//create statement
					st1.executeUpdate(query1);
					st1.close();
					con1.close();
					JOptionPane.showMessageDialog(this, "Success !!!");
					Login l = new Login();
					l.setVisible(true);
					this.setVisible(false);
				}
				catch(Exception e)
				{
					System.out.println(e.getMessage());
					JOptionPane.showMessageDialog(this, "Oops !!!");
				}
					
				}
				else{}
			
		}
			if(!flag)
			{
				JOptionPane.showMessageDialog(this,"Invalid  Password"); 
			}		
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
	}
}