import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import java.sql.*;
import java.util.*;
import java.awt.*;

public class Login extends JFrame implements ActionListener
{
	JLabel title, userLabel, passLabel,addLabel,im;
	JTextField userTF;
	JPasswordField passPF;
	JButton loginBtn, exitBtn,signupBtn;
	JPanel panel;
	private ImageIcon i;
	private Font f1,f2,f3,f4;
	
	public Login()
	{
		super("                                              Login Window                 ");
		
		this.setSize(800, 450);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		int jFrameWidth = this.getSize().width;
		int jFrameHeight = this.getSize().height;
		
		int locationX = (d.width-jFrameWidth)/2;
		int locationY = (d.height-jFrameHeight)/2;
		this.setLocation(locationX,locationY);
		
		panel = new JPanel();
		panel.setLayout(null);
		
		f1=new Font("Arial Rounded MT Bold",Font.BOLD,20);
		f2=new Font("Bahnschrift SemiBold",Font.BOLD,17);
		f3=new Font("Calibri",Font.BOLD,17);
		f4=new Font("Calibri",Font.BOLD,17);
	    
		title = new JLabel("Library Management System");
		title.setBounds(250, 50, 350, 30);
		title.setForeground(Color.WHITE);
		title.setFont(f1);
		panel.add(title);
		
		userLabel = new JLabel("User ID : ");
		userLabel.setBounds(280, 100, 200, 30);
		userLabel.setForeground(Color.WHITE);
		userLabel.setFont(f3);
		panel.add(userLabel);
		
		userTF = new JTextField();
		userTF.setBounds(370, 100, 100, 30);
		panel.add(userTF);
		
		passLabel = new JLabel("Password: ");
		passLabel.setBounds(280, 150, 200, 30);
		passLabel.setForeground(Color.WHITE);
		passLabel.setFont(f4);
		panel.add(passLabel);
		
		passPF = new JPasswordField();
		passPF.setBounds(370, 150, 100, 30);
		panel.add(passPF);
		
		loginBtn = new JButton("Login");
		loginBtn.setBounds(300, 200, 80, 30);
		loginBtn.addActionListener(this);
		panel.add(loginBtn);
		
		
		exitBtn = new JButton("Exit");
		exitBtn.setBounds(390, 200, 80, 30);
		exitBtn.addActionListener(this);
		panel.add(exitBtn);
		
		addLabel = new JLabel("REGISTER AS CUSTOMER??");
		addLabel.setBounds(315, 320, 300, 30);
		addLabel.setForeground(new Color(204,0,0));
		addLabel.setFont(f2);
		//addLabel.setForeground(0xffffffff);
		panel.add(addLabel);
		
		signupBtn = new JButton("Sign up");
		signupBtn.setBounds(350, 350, 80, 30);
		signupBtn.addActionListener(this);
		panel.add(signupBtn);
		
		i = new ImageIcon("40598976_1797979303633028_8299312288431603712_n.jpg");
		im = new JLabel(i);
		im.setBounds(0,0,800,450);
		panel.add(im);
		
		this.add(panel);
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		String text = ae.getActionCommand();
		
		if(text.equals(loginBtn.getText()))
		{
			checkLogin();
		}
		else if(text.equals(exitBtn.getText()))
		{
			System.exit(0);
		}
		else if(text.equals(signupBtn.getText()))
		{
			signup su = new signup();
			su.setVisible(true);
			this.setVisible(false);
		}
		else{}
	}
	
	public void checkLogin()
	{
		String query = "SELECT `userId`, `password`, `status` FROM `login`;";     
        Connection con=null;//for connection
        Statement st = null;//for query execution
		ResultSet rs = null;//to get row by row result from DB
		System.out.println(query);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");//load driver
			System.out.println("driver loaded");
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22","root","");
			System.out.println("connection done");//connection with database established
			st = con.createStatement();//create statement
			System.out.println("statement created");
			rs = st.executeQuery(query);//getting result
			System.out.println("results received");
			
			boolean flag = false;			
			while(rs.next())
			{
                String userId = rs.getString("userId");
                String password = rs.getString("password");
				int status = rs.getInt("status");
				
				if(userId.equals(userTF.getText()) && password.equals(passPF.getText()))
				{
					flag=true;
					if(status==0)
					{
						EmployeeHome eh = new EmployeeHome(userId);
						eh.setVisible(true);
						this.setVisible(false);
					}
					else if(status==1)
					{
						CustomerHome ch = new CustomerHome(userId);
						ch.setVisible(true);
						this.setVisible(false);
					}
					else{}
				}
			}
			if(!flag)
			{
				JOptionPane.showMessageDialog(this,"Invalid ID or Password"); 
			}
		}
        catch(Exception ex)
		{
			System.out.println("Exception : " +ex.getMessage());
        }
        finally
		{
            try
			{
                if(rs!=null)
					rs.close();

                if(st!=null)
					st.close();

                if(con!=null)
					con.close();
            }
            catch(Exception ex){}
        }
	}
	}
