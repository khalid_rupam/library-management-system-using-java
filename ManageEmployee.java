import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.sql.*;

public class ManageEmployee extends JFrame implements ActionListener
{
	
	JLabel imgLabel;
	JButton addEmployeeBtn, viewEmployeeBtn, backBtn, logoutBtn;
	ImageIcon img;
	JPanel panel;
	String userId;
	
	public ManageEmployee(String userId)
	{
		super("Library Management System - Manage Employee Window");
		
		this.userId = userId;
		this.setSize(800,450);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		int jFrameWidth = this.getSize().width;
		int jFrameHeight = this.getSize().height;
		
		int locationX = (d.width-jFrameWidth)/2;
		int locationY = (d.height-jFrameHeight)/2;
		this.setLocation(locationX,locationY);
		
		panel = new JPanel();
		panel.setLayout(null);
		
		logoutBtn = new JButton("Logout");
		logoutBtn.setBounds(600, 50, 100, 30);
		logoutBtn.addActionListener(this);
		panel.add(logoutBtn);
		
		addEmployeeBtn = new JButton("Add Employee");
		addEmployeeBtn.setBounds(300, 120, 150, 30);
		addEmployeeBtn.addActionListener(this);
		panel.add(addEmployeeBtn);
		
		viewEmployeeBtn = new JButton("View Employee");
		viewEmployeeBtn.setBounds(300, 200, 150, 30);
		viewEmployeeBtn.addActionListener(this);
		panel.add(viewEmployeeBtn);
		
		backBtn = new JButton("Back");
		backBtn.setBounds(300, 280, 150, 30);
		backBtn.addActionListener(this);
		panel.add(backBtn);
		
		img =  new ImageIcon("bg op2.jpg");
		imgLabel = new JLabel(img);
		imgLabel.setBounds(0,0,800,450);
		panel.add(imgLabel);
			
		this.add(panel);
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		String text = ae.getActionCommand();
		
		if(text.equals(backBtn.getText()))
		{
			EmployeeHome eh = new EmployeeHome(userId);
			eh.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(logoutBtn.getText()))
		{
			Login lg = new Login();
			lg.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(addEmployeeBtn.getText()))
		{
			AddEmployee a = new AddEmployee(userId);
			a.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(viewEmployeeBtn.getText()))
		{
			ViewEmployee ve = new ViewEmployee(userId);
			ve.setVisible(true);
			this.setVisible(false);
		}
		else{}
	}
}