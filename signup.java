import java.lang.*;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.sql.*;
import java.util.*;

public class signup extends JFrame implements ActionListener
{
	JLabel userLabel, passLabel, cNameLabel, phoneLabel, addressLabel,title,imgLabel;
	JTextField userTF, passTF, phoneTF1, phoneTF2, cNameTF, addressTF;
	JComboBox roleCombo;
	JButton autoPassBtn, addBtn, backBtn, logoutBtn;
	ImageIcon img;
	Font f1,f2;
	JPanel panel;
	
	String userId;
	
	public signup()
	{
		super("Customer Sign Up");
		
		this.setSize(800,500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.userId = userId;
		
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		int jFrameWidth = this.getSize().width;
		int jFrameHeight = this.getSize().height;
		
		int locationX = (d.width-jFrameWidth)/2;
		int locationY = (d.height-jFrameHeight)/2;
		this.setLocation(locationX,locationY);
		
		panel = new JPanel();
		panel.setLayout(null);
		
		f1 = new Font("Calibri",Font.PLAIN,30);
		f2 = new Font("Calibri",Font.BOLD,20);
		
		title = new JLabel("Fill Up The Following Data");
		title.setBounds(230, 50, 350, 30);
		title.setFont(f1);
		panel.add(title);
		
		userLabel = new JLabel("User ID : ");
		userLabel.setBounds(250, 100, 120, 30);
		userLabel.setFont(f2);
		panel.add(userLabel);
		
		Random r = new Random();
		
		userTF = new JTextField("c"+r.nextInt(900));
		userTF.setBounds(400, 100, 120, 30);
		userTF.setForeground(Color.RED);
		userTF.setForeground(Color.RED);
		userTF.setEnabled(false);
		panel.add(userTF);
		
		passLabel = new JLabel("Password : ");
		passLabel.setBounds(250, 150, 120, 30);
		passLabel.setFont(f2);
		panel.add(passLabel);
		
		passTF = new JTextField();
		passTF.setBounds(400, 150, 120, 30);
		panel.add(passTF);
		
		cNameLabel = new JLabel("Name : ");
		cNameLabel.setBounds(250, 200, 120, 30);
		cNameLabel.setFont(f2);
		panel.add(cNameLabel);
		
		cNameTF = new JTextField();
		cNameTF.setBounds(400, 200, 120, 30);
		panel.add(cNameTF);
		
		phoneLabel = new JLabel("Phone No. : ");
		phoneLabel.setBounds(250, 250, 120, 30);
		phoneLabel.setFont(f2);
		panel.add(phoneLabel);
		
		phoneTF1 = new JTextField("+880");
		phoneTF1.setBounds(400, 250, 35, 30);
		phoneTF1.setEnabled(false);
		phoneTF1.setForeground(Color.BLACK);
		panel.add(phoneTF1);
		
		phoneTF2 = new JTextField();
		phoneTF2.setBounds(435, 250, 85, 30);
		panel.add(phoneTF2);
		
		addressLabel = new JLabel("Address : ");
		addressLabel.setBounds(250, 300, 120, 30);
		addressLabel.setFont(f2);
		panel.add(addressLabel);
		
		addressTF = new JTextField();
		addressTF.setBounds(400, 300, 120, 30);
		panel.add(addressTF);
		
		addBtn = new JButton("Add");
		addBtn.setBounds(250, 400, 120, 30);
		addBtn.addActionListener(this);
		panel.add(addBtn);
		
		backBtn = new JButton("Back");
		backBtn.setBounds(400, 400, 120, 30);
		backBtn.addActionListener(this);
		panel.add(backBtn);
		
		img =  new ImageIcon("bg op2.jpg");
		imgLabel = new JLabel(img);
		imgLabel.setBounds(0,0,800,500);
		panel.add(imgLabel);
		
		this.add(panel);
	}
	
	public void actionPerformed(ActionEvent ae)
	{
		String text = ae.getActionCommand();
		
		if(text.equals(backBtn.getText()))
		{
			Login lg = new Login();
			lg.setVisible(true);
			this.setVisible(false);
		}
		else if(text.equals(addBtn.getText()))
		{
			insertIntoDB();
		}
		else{}
	}
	public void insertIntoDB()
	{
		String newId = userTF.getText();
		String newPass = passTF.getText();
		String cName = cNameTF.getText();
		//String phnNo = phoneTF1.getText()+phoneTF2.getText();
		String address = addressTF.getText();
		int status = 1;
		int phn = 0;
		String phnNo=null;
		int count=0;
		try{
		 phn= Integer.parseInt(phoneTF2.getText());
		 phnNo = phoneTF1.getText()+Integer.toString(phn);
		 count = 1;
		}
		catch(Exception e)
		{
			JOptionPane.showMessageDialog(this, "Oops !!!error number!!!!!");
		}
		if(count == 1)
		{
		String query1 = "INSERT INTO customer VALUES ('"+newId+"','"+cName+"','"+ phnNo+"','"+address+"');";
		String query2 = "INSERT INTO login VALUES ('"+newId+"','"+newPass+"',"+status+");";
		System.out.println(query1);
		System.out.println(query2);
        try
		{
			Class.forName("com.mysql.jdbc.Driver");
			System.out.println("OK");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/e22", "root", "");
			System.out.println("OK");
			Statement stm = con.createStatement();
			System.out.println("OK");
			stm.execute(query1);
			System.out.println("OK");
			stm.execute(query2);
			System.out.println("OK");
			stm.close();
			System.out.println("OK");
			con.close();
			System.out.println("OK");
			JOptionPane.showMessageDialog(this, "Success !!!");
		}
        catch(Exception ex)
		{
			JOptionPane.showMessageDialog(this, "Oops !!!");
        }
		Login l = new Login();
		l.setVisible(true);
		this.setVisible(false);
		}
		else
		{
			this.setVisible(false);
			this.setVisible(true);
		}
    }	
}